package com.example.albert.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Main extends AppCompatActivity {

    EditText eT;
    Double num,num1,num2,res;
    boolean first,newentry;
    String str;
    int op;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eT=(EditText)findViewById(R.id.eT);

        first=true;
        newentry=false;
        op=0;
    }

    public void plus(View view) {
        if (newentry){
            str=eT.getText().toString();
            if (str=="" || str=="." || str=="-"){
                eT.setText("");
                Toast.makeText(this, "Please enter number", Toast.LENGTH_LONG).show();
            } else {
                num = Double.parseDouble(str);
                op = 1;
                if (first) {
                    first = false;
                    num1 = num;
                } else {
                    num2 = num;
                    res = num1 + num2;
                    eT.setText("" + res);
                    num1 = res;
                }
                newentry=false;
            }
        } else {
            if (first) {
                Toast.makeText(this, "Please enter number", Toast.LENGTH_LONG).show();
            } else {
                res = num1 + num2;
                eT.setText("" + res);
                num1 = res;
            }
        }
    }

    public void minus(View view) {
        str=eT.getText().toString();
        if (str==null) {
            Toast.makeText(this, "Please enter number", Toast.LENGTH_LONG).show();
        }
        else {
            num=Double.parseDouble(str);
            op=2;
            if (first){
                first=false;
                res=num;
            }
            else {
                res=res-num;
                eT.setText(""+res);
                num1=res;
            }
        }
    }

    public void mult(View view) {
        str=eT.getText().toString();
        if (str==null) {
            Toast.makeText(this, "Please enter number", Toast.LENGTH_LONG).show();
        }
        else {
            num=Double.parseDouble(str);
            op=3;
            if (first){
                first=false;
                res=num;
            }
            else {
                res=res*num;
                eT.setText(""+res);
                num1=res;
            }
        }
    }

    public void div(View view) {
        str=eT.getText().toString();
        if (str==null) {
            Toast.makeText(this, "Please enter number", Toast.LENGTH_LONG).show();
        }
        else {
            num=Double.parseDouble(str);
            op=4;
            if (first){
                first=false;
                res=num;
            }
            else {
                res=res*num;
                eT.setText(""+res);
                num1=res;
            }
        }
    }

    public void ac(View view) {
        first=true;
        num=0.0;
        num1=0.0;
        num2=0.0;
        res=0.0;
        eT.setText("");
    }

    public void equal(View view) {
        str=eT.getText().toString();
        if (str==null) {
            Toast.makeText(this, "Please enter number", Toast.LENGTH_LONG).show();
        }
        else {
            num=Double.parseDouble(str);
            if (first==false){
                switch (op){
                    case 1: res=res+num;
                        break;
                    case 2: res=res-num;
                        break;
                    case 3: res=res*num;
                        break;
                    case 4: res=res/num;
                        break;
                }
                op=0;
                eT.setText(""+res);
                num1=res;
            }
            else {
                res=num;
            }
        }
    }

    public void credits(View view) {
    }

    public void clear(View view) {
        eT.setText("");
        newentry=true;
    }
}
